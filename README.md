# Phone Service App

The site implements a mobile phone service application. It is possible to register, and after activation you can use the full function of the site. You are able to upload a photo and provide a description about  the issues of the device. After uploading, it is possible to track the status of the service.

## Installation

At first, you should install the environment for the application.

You have to [download](https://maven.apache.org/download.cgi) and [install](https://maven.apache.org/install.html) maven, which can build and manage the project.
It is also necessary to install Java environment. You have to install minimum 1.8 version from Java. You can [download](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html) it and [install](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html) it for your computer.

## Usage

```java
mvn clean install  //when you find pom.xml

java -jar target/phoneservice-0.0.1-SNAPSHOT.jar
```

## Technologies used:

* Backend:
	* Spring boot
	* Spring Security
	* Spring Web
	* Spring boot mail
	* PostgreSQL (pgcrypto)
	* JPA
	* Thymeleaf - Spring Security 5
	* Jasypt (Encrypt)
	* Tomcat
	* Lombok
	* Checkstyle
	* Spotbugs

* Frontend
	* Bootstrap
	* HTML 5
	* CSS3
	* JavaScript

