package hu.phoneservice.phoneservice.validator;

import hu.phoneservice.phoneservice.service.validator.EmailFormatValidator;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class EmailFormatValidatorTest {

    private EmailFormatValidator underTest = new EmailFormatValidator();
    
    @Test
    public void testEmailFormatValidatorWithCorrectEmailAddress(){
        String email = "test@test.hu";
        boolean result = underTest.isValid(email);
        assertTrue(result);
    }
    
    @Test
    public void testEmailFormatValidatorWithIncorrectEmailAddress(){
        String email = "something";
        boolean result = underTest.isValid(email);
        assertFalse(result);
    }
}
