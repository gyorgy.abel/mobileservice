package hu.phoneservice.phoneservice.validator;

import hu.phoneservice.phoneservice.service.validator.UuidValidator;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class KeyValidatorTest {

    private UuidValidator underTest = new UuidValidator();
    
    @Test
    public void KeyValidatorWithCorrectUUID(){
        String key = "123e4567-e89b-12d3-a456-426614174000";
        boolean result = underTest.isValidKey(key);
        assertTrue(result);
    }
    
    @Test
    public void KeyValidatorWithIncorrectUUID(){
        String key = "123e4567-e89b-12d3a456426614174000";
        boolean result = underTest.isValidKey(key);
        assertFalse(result);
    }
}
