package hu.phoneservice.phoneservice.validator;

import hu.phoneservice.phoneservice.service.validator.NameFormatValidator;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class NameFormatValidatorTest {

    private NameFormatValidator underTest = new NameFormatValidator();
    
    @Test
    public void NameFormatValidatorWithCorrectName(){
        String name = "George";
        boolean result = underTest.isValidName(name);
        assertTrue(result);
    }
    
    @Test
    public void NameFormatValidatorWithIncorrectName(){
        String name = "George2";
        boolean result = underTest.isValidName(name);
        assertFalse(result);
    }
}
