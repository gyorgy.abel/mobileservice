package hu.phoneservice.phoneservice.validator;

import hu.phoneservice.phoneservice.service.validator.EmailFormatValidator;
import hu.phoneservice.phoneservice.service.validator.InputValidator;
import hu.phoneservice.phoneservice.service.validator.NameFormatValidator;
import hu.phoneservice.phoneservice.service.validator.PasswordFormatValidator;
import hu.phoneservice.phoneservice.service.validator.UuidValidator;
import org.springframework.boot.test.context.SpringBootTest;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

@SpringBootTest
public class InputValidatorTest {

    private InputValidator underTest = new InputValidator();
    
    @Mock
    private EmailFormatValidator emailFormatValidator;
    
    @Mock
    private UuidValidator keyValidator;
    
    @Mock
    private NameFormatValidator nameValidator;
    
    @Mock
    private PasswordFormatValidator passwordFormatValidator;
    
    @BeforeEach
    void init(){
        underTest.setEmailFormatValidator(emailFormatValidator);
        underTest.setKeyValidator(keyValidator);
        underTest.setNameValidator(nameValidator);
        underTest.setPasswordValidator(passwordFormatValidator);
    }
    
    @Test
    public void EmailFormatValidatorTestWithCorrectEmailAddress(){
        String email = "test@test.hu";
        when(emailFormatValidator.isValid(email)).thenReturn(true);
        String result = underTest.emailCheck(email);
        assertTrue(result.isEmpty());
    }
    
    @Test
    public void EmailFormatValidatorTestWithIncorrectEmailAddress(){
        String email = "test";
        when(emailFormatValidator.isValid(email)).thenReturn(false);
        String result = underTest.emailCheck(email);
        assertEquals("email_format_not_correct", result);
    }
    
    @Test
    public void EmailFormatValidatorTestWithEmptyEmailAddress(){
        String email = null;
        when(emailFormatValidator.isValid(email)).thenReturn(false);
        String result = underTest.emailCheck(email);
        assertEquals("email_required", result);
    }
    
    @Test
    public void KeyValidatorTestWithCorrectKey(){
        String key = "123e4567-e89b-12d3-a456-426614174000";
        when(keyValidator.isValidKey(key)).thenReturn(true);
        String result = underTest.keyCheck(key);
        assertTrue(result.isEmpty());
    }
    
    @Test
    public void KeyValidatorTestWithIncorrectKey(){
        String key = "123e4567-e89b-12d3a456426614174000";
        when(keyValidator.isValidKey(key)).thenReturn(false);
        String result = underTest.keyCheck(key);
        assertEquals("key_format_not_correct", result);
    }
    
    @Test
    public void KeyValidatorTestWithEmptyKey(){
        String key = null;
        when(keyValidator.isValidKey(key)).thenReturn(false);
        String result = underTest.keyCheck(key);
        assertEquals("key_not_empty", result);
    }
    
    @Test
    public void NameValidatorTestWithCorrectName(){
        String name = "George";
        when(nameValidator.isValidName(name)).thenReturn(true);
        String result = underTest.nameCheck(name);
        assertTrue(result.isEmpty());
    }
    
    @Test
    public void NameValidatorTestWithIncorrectName(){
        String name = "George12345";
        when(nameValidator.isValidName(name)).thenReturn(false);
        String result = underTest.nameCheck(name);
        assertEquals("name_format_not_correct", result);
    }
    
    @Test
    public void NameValidatorTestWithEmptyName(){
        String name = null;
        when(nameValidator.isValidName(name)).thenReturn(false);
        String result = underTest.nameCheck(name);
        assertEquals("name_required", result);
    }
    
    @Test
    public void PasswordFormatValidatorTestWithCorrectPassword(){
        String password = "TestPassword123456";
        String confirmPassword = "TestPassword123456";
        when(passwordFormatValidator.isValidPassword(password)).thenReturn(true);
        String result = underTest.newPasswordCheck(password, confirmPassword);
        assertTrue(result.isEmpty());
    }
    
    @Test
    public void PasswordFormatValidatorTestWithIncorrectPassword(){
        String password = "password";
        String confirmPassword = "password";
        when(passwordFormatValidator.isValidPassword(password)).thenReturn(false);
        String result = underTest.newPasswordCheck(password, confirmPassword);
        assertEquals("password_format_not_correct", result);
    }
    
    @Test
    public void PasswordFormatValidatorTestWithNotEqualsPassword(){
        String password = "TestPassword123456";
        String confirmPassword = "TestPassword1234567";
        when(passwordFormatValidator.isValidPassword(password)).thenReturn(true);
        String result = underTest.newPasswordCheck(password, confirmPassword);
        assertEquals("two_password_not_equals", result);
    }
    
    @Test
    public void PasswordFormatValidatorTestWithEmptyPassword(){
        String password = null;
        String confirmPassword = null;
        when(passwordFormatValidator.isValidPassword(password)).thenReturn(false);
        String result = underTest.newPasswordCheck(password, confirmPassword);
        assertEquals("password_required", result);
    }
}
