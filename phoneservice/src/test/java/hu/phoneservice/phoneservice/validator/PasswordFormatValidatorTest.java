package hu.phoneservice.phoneservice.validator;

import hu.phoneservice.phoneservice.service.validator.PasswordFormatValidator;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class PasswordFormatValidatorTest {

    private PasswordFormatValidator underTest = new PasswordFormatValidator();
    
    @Test
    public void PasswordFormatValidatorTestWithCorrectPassword(){
        String password = "TestPassword123456";
        boolean result = underTest.isValidPassword(password);
        assertTrue(result);
    }
    
    @Test
    public void PasswordFormatValidatorTestWithIncorrectPassword(){
        String password = "password";
        boolean result = underTest.isValidPassword(password);
        assertFalse(result);
    }
}
