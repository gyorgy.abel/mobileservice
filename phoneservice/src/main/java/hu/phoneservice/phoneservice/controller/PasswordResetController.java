package hu.phoneservice.phoneservice.controller;

import hu.phoneservice.phoneservice.dto.UserDto;
import hu.phoneservice.phoneservice.service.user.ResetPasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PasswordResetController {

    private ResetPasswordService resetPasswordService;
    
    @Autowired
    public PasswordResetController(ResetPasswordService resetPasswordService) {
        this.resetPasswordService = resetPasswordService;
    }

    @RequestMapping(path = "/forgot-password", method = RequestMethod.GET)
    public String forgotPassword() {
        return "forgot-password";
    }
    
    @RequestMapping(path = "/forgot-password-sender", method = RequestMethod.GET)
    public String getForgotPassword() {
        return "forgot-password";
    }

    @RequestMapping(path = "/forgot-password-sender", method = RequestMethod.POST)
    public String forgotPassword(Model model, @RequestParam String email) {
        String result = resetPasswordService.newPasswordSender(email);
        model.addAttribute("forgotPassword", result);
        return "forgot-password";
    }

    @RequestMapping(path = "/password-reset", method = RequestMethod.GET)
    public String resetPasswordGet(Model model, @ModelAttribute("user") UserDto user) {
        if (user.getEmailAddress() != null && !user.getEmailAddress().isEmpty()) {
            model.addAttribute("user", user);
            return "password-reset";
        }
        model.addAttribute("passwordReset", "key_not_empty");
        return "password-reset";
    }

    @RequestMapping(path = "/password-reset/{code}", method = RequestMethod.GET)
    public String resetPasswordCodeCheck(@PathVariable("code") String code,
            @ModelAttribute("user") UserDto user,
            Model model) {
        String result = resetPasswordService.forgotPasswordKeyCheck(code);
        if (!result.contains("@")) {
            model.addAttribute("passwordReset", result);
            return "password-reset";
        }
        user.setEmailAddress(result);
        model.addAttribute("user", user);
        return "password-reset";
    }

    @RequestMapping(path = "/password-restore", method = RequestMethod.POST)
    public String resetPasswordPost(Model model, @ModelAttribute("user") UserDto user) {
        String result = resetPasswordService.passwordReset(user);
        if (!"password_reset_success".equals(result)) {
            model.addAttribute("user", user);
            model.addAttribute("passwordReset", result);
            return "password-reset";
        }
        model.addAttribute("passwordReset", result);
        return "auth/login";
    }
}
