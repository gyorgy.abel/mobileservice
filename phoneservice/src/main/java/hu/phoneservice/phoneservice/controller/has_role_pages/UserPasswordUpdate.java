package hu.phoneservice.phoneservice.controller.has_role_pages;

import hu.phoneservice.phoneservice.service.user.UserPasswordUpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class UserPasswordUpdate {
    
    private UserPasswordUpdateService userPasswordUpdate;
    
    @Autowired
    public UserPasswordUpdate(UserPasswordUpdateService userPasswordUpdate) {
        this.userPasswordUpdate = userPasswordUpdate;
    }
    
    @RequestMapping(value = "/password-update", method = RequestMethod.GET)
    public String changeUserDatas() {
        
        return "password-update";
    }
    
    @RequestMapping(value = "/password-update", method = RequestMethod.POST)
    public String updateUserDatas(Model model, @RequestParam String oldPassword, String newPassword, String confirmNewPassword) {  
        String passwordUpdateResult = userPasswordUpdate.passwordUpdate(oldPassword, newPassword, confirmNewPassword);
        model.addAttribute("passwordUpdate", passwordUpdateResult);
        return "password-update";
    }
}
