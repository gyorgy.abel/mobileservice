package hu.phoneservice.phoneservice.controller;

import hu.phoneservice.phoneservice.service.user.UserActivationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AccountActivationController {

    private UserActivationService userActivation;

    @Autowired
    public AccountActivationController(UserActivationService userActivation) {
        this.userActivation = userActivation;
    }
    
    @RequestMapping(path = "/activation", method = RequestMethod.GET)
    public String activation(Model model) {
        model.addAttribute("activation", "activation_code_required");
        return "auth/login";
    }
    
    @RequestMapping(path = "/activation/{code}", method = RequestMethod.GET)
    public String activation(@PathVariable("code") String code, Model model) {
        String result = userActivation.userActivation(code);
        model.addAttribute("activation", result);
        return "auth/login";
    }
}
