package hu.phoneservice.phoneservice.controller;

import hu.phoneservice.phoneservice.dto.UserDto;
import hu.phoneservice.phoneservice.service.user.UserRegistrationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RegistrationController {
    
    private UserRegistrationService userRegistration;
    
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public RegistrationController(UserRegistrationService userRegistration) {
        this.userRegistration = userRegistration;
    }

    @RequestMapping(value = "/user-registration", method = RequestMethod.GET)
    public String userRegistration(Model model) {
        model.addAttribute("user", new UserDto());
        return "registration/user-registration";
    }

    @RequestMapping(value = "/user-registration", method = RequestMethod.POST)
    public String userReg(Model model, @ModelAttribute("user") UserDto user) {

        String userRegistrationMessage = userRegistration.registerNewUser(user);
        model.addAttribute("user_registration_message", userRegistrationMessage);

        if (!"registration_success".equals(userRegistrationMessage)) {
            return "registration/user-registration";
        }
        log.info("new user");
        return "auth/login";
    }
}
