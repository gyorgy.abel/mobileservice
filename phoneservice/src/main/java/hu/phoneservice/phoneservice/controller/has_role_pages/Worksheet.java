package hu.phoneservice.phoneservice.controller.has_role_pages;

import hu.phoneservice.phoneservice.dto.WorksheetDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class Worksheet {

    @RequestMapping(value = "/worksheet", method = RequestMethod.GET)
    public String getWorksheetPage(Model model) {
        model.addAttribute("worksheet", new WorksheetDto());
        return "worksheet";
    }
    
    @RequestMapping(value = "/new-worksheet", method = RequestMethod.GET)
    public String getWorksheet(Model model) {
        model.addAttribute("worksheet", new WorksheetDto());
        return "worksheet";
    }
    
    @RequestMapping(value = "/new-worksheet", method = RequestMethod.POST)
    public String newWorksheet(Model model, @ModelAttribute("worksheet") WorksheetDto worksheet) {
        
        return "worksheet";
    }
    
    @RequestMapping(value = "/worksheet-update", method = RequestMethod.GET)
    public String worksheetUpdatePage() {
        
        return "worksheet";
    }
    
    @RequestMapping(value = "/worksheet-update", method = RequestMethod.POST)
    public String getWorksheetUpdatePage() {
        
        return "worksheet-update";
    }
}
