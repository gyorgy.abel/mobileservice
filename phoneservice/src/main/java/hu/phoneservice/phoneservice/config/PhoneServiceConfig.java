package hu.phoneservice.phoneservice.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@EntityScan(basePackages = {"hu.phoneservice.phoneservice.entity"})
@PropertySource({
    "classpath:db/db.properties",
    "classpath:db/db.encryption.pw.properties",
    "classpath:mail/mail.properties",
    "classpath:pw/pw.properties"
})
public class PhoneServiceConfig {}
