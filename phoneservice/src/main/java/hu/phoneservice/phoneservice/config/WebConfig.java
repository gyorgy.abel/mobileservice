package hu.phoneservice.phoneservice.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addViewControllers(ViewControllerRegistry customView) {
        WebMvcConfigurer.super.addViewControllers(customView);
        customView.addViewController("/login").setViewName("auth/login");
        customView.setOrder(Ordered.HIGHEST_PRECEDENCE);
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
