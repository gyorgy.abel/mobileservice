package hu.phoneservice.phoneservice.dto;

import java.time.ZonedDateTime;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.stereotype.Component;

@Getter
@Setter
@EqualsAndHashCode(of = "userId")
@ToString
@Component
public class UserDto {

    private Long userId;
    private String emailAddress;
    private String password;
    private String confirmPassword;
    private String firstName;
    private String lastName;
    private ZonedDateTime createdAt;
    private ZonedDateTime updatedAt;
    private ZonedDateTime deletedAt;
}
