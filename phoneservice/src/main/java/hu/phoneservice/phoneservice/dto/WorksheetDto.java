package hu.phoneservice.phoneservice.dto;

import hu.phoneservice.phoneservice.entity.User;
import hu.phoneservice.phoneservice.enums.WorkSheetStatus;
import java.time.ZonedDateTime;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.stereotype.Component;

@Getter
@Setter
@EqualsAndHashCode(of = "worksheetId")
@ToString
@Component
public class WorksheetDto {

    private Integer worksheetId;
    private String phoneType;
    private Long imeiNumber;
    private String issueDescription;
    private WorkSheetStatus worksheetStatus;
    private User userId;
    private ZonedDateTime createdAt;
    private ZonedDateTime updatedAt;
    private ZonedDateTime deletedAt;
}
