package hu.phoneservice.phoneservice.repositroy;

import hu.phoneservice.phoneservice.entity.Role;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepositroy extends CrudRepository<Role, Long>{
    
    Optional<Role> findByRole(String Role);
}
