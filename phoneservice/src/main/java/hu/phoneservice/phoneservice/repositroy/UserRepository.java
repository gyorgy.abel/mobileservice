package hu.phoneservice.phoneservice.repositroy;

import hu.phoneservice.phoneservice.entity.User;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long>{

    @Override
    List<User> findAll();
    
    Optional<User> findByEmailAddress(String emailAddress);
    
    Optional<User> findByActivation(String activation);
    
    Optional<User> findByForgotPasswordKey(String forgotPasswordKey);
}
