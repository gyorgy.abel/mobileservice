package hu.phoneservice.phoneservice.exception;

public class UserNotFoundByEmailException extends RuntimeException{

    public String message(String email) {
        return "Nem található a következő email cím: " + email;
    }
}
