package hu.phoneservice.phoneservice.enums;

public enum WorkSheetStatus {
    
    WAITING_OFFER,
    UNTIL_REPAIRE, 
    REPAIRE_COMPLETED,
    DONE
}
