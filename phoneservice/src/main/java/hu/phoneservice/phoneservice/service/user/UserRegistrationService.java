package hu.phoneservice.phoneservice.service.user;

import hu.phoneservice.phoneservice.dto.UserDto;
import hu.phoneservice.phoneservice.entity.Role;
import hu.phoneservice.phoneservice.entity.User;
import hu.phoneservice.phoneservice.generator.UuidGenerator;
import hu.phoneservice.phoneservice.service.validator.InputValidator;
import java.time.ZonedDateTime;
import java.util.Optional;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import hu.phoneservice.phoneservice.repositroy.RoleRepositroy;
import hu.phoneservice.phoneservice.repositroy.UserRepository;
import hu.phoneservice.phoneservice.service.email.UserRegistrationEmailService;

@Service
public class UserRegistrationService {
    
    private UserRepository userRepository;
    private RoleRepositroy roleRepository;
    private PasswordEncoder passwordEncoder;
    private UuidGenerator uuidGenerator;
    private InputValidator inputValidator;
    private ModelMapper modelMapper;
    private UserRegistrationEmailService userRegistrationEmail;
    
    private static final String USER_ROLE = "USER";

    @Autowired
    public UserRegistrationService(UserRepository userRepository, RoleRepositroy roleRepository,
                                   PasswordEncoder passwordEncoder, UuidGenerator uuidGenerator,
                                   InputValidator inputValidator, ModelMapper modelMapper,
                                   UserRegistrationEmailService userRegistrationEmail) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.uuidGenerator = uuidGenerator;
        this.inputValidator = inputValidator;
        this.modelMapper = modelMapper;
        this.userRegistrationEmail = userRegistrationEmail;
    }
    
    
    public String registerNewUser(UserDto user) {
        
        String isValidFirstName = inputValidator.nameCheck(user.getFirstName());        
        if (!isValidFirstName.isEmpty()) {
            return isValidFirstName;
        }
        
        String isValidLastName = inputValidator.nameCheck(user.getLastName());
        if (!isValidLastName.isEmpty()) {
            return isValidLastName;
        }
        
        String isValidEmailAddress = inputValidator.emailCheck(user.getEmailAddress());
        if (!isValidEmailAddress.isEmpty()) {
            return isValidEmailAddress;
        }
        
        String isValidPassword = inputValidator.newPasswordCheck(user.getPassword(), user.getConfirmPassword());
        if (!isValidPassword.isEmpty()) {
            return isValidPassword;
        }

        String email = user.getEmailAddress();
        Optional<User> userCheck = userRepository.findByEmailAddress(email);
        if (userCheck.isPresent()) {
            return "email_already_exists";
        }
        
        User entity = new User();
        modelMapper.map(user, entity);
        
        Optional<Role> userRole = roleRepository.findByRole(USER_ROLE);

        if (userRole.isPresent()) {
            entity.getUserRoles().add(userRole.get());
        } else {
            entity.addRoles(USER_ROLE);
        }

        String activationKey = uuidGenerator.uuidGenerator();
        String encodedPassword = passwordEncoder.encode(entity.getPassword());
        entity.setPassword(encodedPassword);
        entity.setActivation(activationKey);
        entity.setCreatedAt(ZonedDateTime.now());
        userRepository.save(entity);
        
        String sendRegEmail = sendEmail(
                entity.getFirstName(),
                entity.getLastName(),
                entity.getEmailAddress(),
                activationKey);
        
        if ("email_sent_error".equals(sendRegEmail)) {
            return "email_sent_error";
        }
        return "registration_success";
    }    
    
    private String sendEmail(String fristName, String lastName, String email, String activationKey) {

        String regEmail;
        
        regEmail = this.userRegistrationEmail.sendRegistrationEmail(
                fristName,
                lastName,
                email,
                activationKey);

        return regEmail;
    }
}
