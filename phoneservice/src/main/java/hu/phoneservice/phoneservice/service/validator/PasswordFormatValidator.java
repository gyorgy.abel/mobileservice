package hu.phoneservice.phoneservice.service.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;

@Component
public class PasswordFormatValidator {

    private Pattern pattern;
    private Matcher matcher;
    //Minimum eight characters, at least one uppercase letter, one lowercase letter and one number
    private static final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$";
    
    public boolean isValidPassword(String password){
        return validateName(password);
    }
    
    private boolean validateName(String password) {
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }
}
