package hu.phoneservice.phoneservice.service.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class UserRegistrationEmailService {

    @Value("${spring.mail.username}")
    private String from;
    private String subject;
    private String message;
    private EmailSenderService emailSender;

    @Autowired
    public UserRegistrationEmailService(EmailSenderService emailSender) {
        this.emailSender = emailSender;
    }
    
    public String sendRegistrationEmail(String firstName, String lastName, String to, String activationKey){
        
        this.message=("Kedves " + firstName + " " + lastName + "! \n \n"
                + "Köszönjük hogy regisztrált oldalunkra mint felhasználó. \n \n"
                + "A következő linken tudja aktiválni fiókját: \n \n"
                + "http://localhost:8080/activation/"+ activationKey + "\n \n"
                + "Üdvözlettel MobileService csapata!");
        
        this.subject = "Regisztráció megerősítése!";

        return emailSender.sendEmail(this.from, to, this.subject, this.message);
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setEmailSender(EmailSenderService emailSender) {
        this.emailSender = emailSender;
    }
}
