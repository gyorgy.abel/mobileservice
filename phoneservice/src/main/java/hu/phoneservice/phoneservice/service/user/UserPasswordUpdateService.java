package hu.phoneservice.phoneservice.service.user;

import hu.phoneservice.phoneservice.entity.User;
import hu.phoneservice.phoneservice.repositroy.UserRepository;
import hu.phoneservice.phoneservice.service.validator.InputValidator;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserPasswordUpdateService {

    private PasswordEncoder passwordEncoder;
    private InputValidator inputValidator;
    private UserRepository userRepository;

    @Autowired
    public UserPasswordUpdateService(PasswordEncoder passwordEncoder, InputValidator inputValidator, UserRepository userRepository) {
        this.passwordEncoder = passwordEncoder;
        this.inputValidator = inputValidator;
        this.userRepository = userRepository;
    }

    public String passwordUpdate(String oldPassword, String newPassword, String confirmNewPassword) {

        String isValidOldPassword = inputValidator.passwordCheck(oldPassword);
        if (!isValidOldPassword.isEmpty()) {
            return isValidOldPassword;
        }

        String isValidPassword = inputValidator.newPasswordCheck(newPassword, confirmNewPassword);
        if (!isValidPassword.isEmpty()) {
            return isValidPassword;
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalEmailAddress = authentication.getName();

        Optional<User> currentLoggedInUser = userRepository.findByEmailAddress(currentPrincipalEmailAddress);

        if (currentLoggedInUser.isPresent()) {
            if (!passwordEncoder.matches(oldPassword, currentLoggedInUser.get().getPassword())) {
                return "incorrect_old_password";
            }
        } else {
            return "error_loading_datas";
        }

        currentLoggedInUser.get().setPassword(passwordEncoder.encode(newPassword));
        userRepository.save(currentLoggedInUser.get());

        return "password_update_success";
    }

    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public void setInputValidator(InputValidator inputValidator) {
        this.inputValidator = inputValidator;
    }

    public void setUserDao(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

}
