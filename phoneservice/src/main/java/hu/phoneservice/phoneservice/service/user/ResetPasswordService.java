package hu.phoneservice.phoneservice.service.user;

import hu.phoneservice.phoneservice.dto.UserDto;
import hu.phoneservice.phoneservice.entity.User;
import hu.phoneservice.phoneservice.generator.UuidGenerator;
import hu.phoneservice.phoneservice.repositroy.UserRepository;
import hu.phoneservice.phoneservice.service.email.ForgotPasswordEmailService;
import hu.phoneservice.phoneservice.service.validator.InputValidator;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class ResetPasswordService {

    private InputValidator inputValidator;
    private UserRepository userRepository;
    private ForgotPasswordEmailService forgotPasswordEmail;
    private UuidGenerator uuidGenerator;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public ResetPasswordService(InputValidator inputValidator, UserRepository userRepository,
                                ForgotPasswordEmailService forgotPasswordEmail,
                                UuidGenerator uuidGenerator, PasswordEncoder passwordEncoder) {
        this.inputValidator = inputValidator;
        this.userRepository = userRepository;
        this.forgotPasswordEmail = forgotPasswordEmail;
        this.uuidGenerator = uuidGenerator;
        this.passwordEncoder = passwordEncoder;
    }

    public String newPasswordSender(String emailAddress) {

        String isValidEmail = inputValidator.emailCheck(emailAddress);

        if (!isValidEmail.isEmpty()) {
            return isValidEmail;
        }

        Optional<User> user = userRepository.findByEmailAddress(emailAddress);
        if (!user.isPresent()) {
            return "email_address_not_found";
        }

        String passwordResetKey = uuidGenerator.uuidGenerator();
        user.get().setForgotPasswordKey(passwordResetKey);
        userRepository.save(user.get());
        
        String mail = 
            forgotPasswordEmail.sendNewPasswordEmail(
                user.get().getFirstName(),
                user.get().getLastName(),
                user.get().getEmailAddress(),
                passwordResetKey);
        
        return mail;
    }    
    
    public String passwordReset(UserDto user){
    
        String passwordValidator = inputValidator.newPasswordCheck(user.getPassword(), user.getConfirmPassword());
        String emailValidator = inputValidator.emailCheck(user.getEmailAddress());
        
        if (!passwordValidator.isEmpty()) {
            return passwordValidator;
        }
        
        if (!emailValidator.isEmpty()) {
            return emailValidator;
        }
        
        Optional<User> userByEmail = userRepository.findByEmailAddress(user.getEmailAddress());
        
        if (!userByEmail.isPresent()) {
            return "email_not_found";
        }
        
        userByEmail.get().setPassword(passwordEncoder.encode(user.getPassword()));
        userByEmail.get().setForgotPasswordKey("");
        userRepository.save(userByEmail.get());
        
        return "password_reset_success";
    }
    
    public String forgotPasswordKeyCheck(String forgotPasswordKey){
        
        String keyCheck = inputValidator.keyCheck(forgotPasswordKey);
        
        if (!keyCheck.isEmpty()) {
            return keyCheck;
        }
                
        Optional<User> checkKeyInDB = userRepository.findByForgotPasswordKey(forgotPasswordKey);
        
        if (!checkKeyInDB.isPresent()) {
            return "key_not_found";
        }
        
        return checkKeyInDB.get().getEmailAddress();
    }
}
