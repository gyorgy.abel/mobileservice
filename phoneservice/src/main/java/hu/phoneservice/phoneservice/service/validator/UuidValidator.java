package hu.phoneservice.phoneservice.service.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;

@Component
public class UuidValidator {

    private Pattern pattern;
    private Matcher matcher;
    private static final String UUID_PATTERN = 
              "[0-9a-f]{8}-"
            + "[0-9a-f]{4}-"
            + "[1-5][0-9a-f]{3}-"
            + "[89ab][0-9a-f]{3}-"
            + "[0-9a-f]{12}";
    
    public boolean isValidKey(String key){
        return validate(key);
    }
    
    private boolean validate(String key) {
        pattern = Pattern.compile(UUID_PATTERN);
        matcher = pattern.matcher(key);
        return matcher.matches();
    }
}
