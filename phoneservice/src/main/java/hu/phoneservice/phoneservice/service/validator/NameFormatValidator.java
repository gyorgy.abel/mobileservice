package hu.phoneservice.phoneservice.service.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Component;

@Component
public class NameFormatValidator {

    private Pattern pattern;
    private Matcher matcher;
    private static final String NAME_PATTERN = "\\p{L}+";
    
    public boolean isValidName(String name){
        return validateName(name);
    }
    
    private boolean validateName(String name) {
        pattern = Pattern.compile(NAME_PATTERN);
        matcher = pattern.matcher(name);
        return matcher.matches();
    }
}
