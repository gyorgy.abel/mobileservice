package hu.phoneservice.phoneservice.service.user;

import hu.phoneservice.phoneservice.exception.UserNotFoundByEmailException;
import hu.phoneservice.phoneservice.entity.User;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import hu.phoneservice.phoneservice.repositroy.UserRepository;

@Service
public class UserByUsernameImplService implements UserDetailsService{

    private UserRepository userRepository;

    @Autowired
    public UserByUsernameImplService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> findByEmail = userRepository.findByEmailAddress(username);

        if (findByEmail.isPresent()) {
            return new UserDetailsImplService(findByEmail.get());
        } else {
            throw new UserNotFoundByEmailException();
        }
    }
}
