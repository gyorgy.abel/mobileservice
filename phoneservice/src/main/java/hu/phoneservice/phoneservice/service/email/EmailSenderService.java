package hu.phoneservice.phoneservice.service.email;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailSenderService {

    private final Log log = LogFactory.getLog(this.getClass());
    
    private JavaMailSender javaMailSender;

    @Autowired
    public EmailSenderService(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }
    
    public String sendEmail(String from, String to, String subject, String text){
        SimpleMailMessage message;
        
        try {
            message = new SimpleMailMessage();
            message.setFrom(from);
            message.setTo(to);
            message.setSubject(subject);
            message.setText(text);
            javaMailSender.send(message);
            return "email_sent";
        } catch (MailException e) {
            log.info("Hiba e-mail küldésekor!" + e);
            return "email_sent_error";
        }
    }
}
