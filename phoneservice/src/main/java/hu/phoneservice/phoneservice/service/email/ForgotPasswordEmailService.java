package hu.phoneservice.phoneservice.service.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ForgotPasswordEmailService {

    @Value("${spring.mail.username}")
    private String from;
    private String subject;
    private String message;
    private EmailSenderService emailSender;

    @Autowired
    public ForgotPasswordEmailService(EmailSenderService emailSender) {
        this.emailSender = emailSender;
    }
    
    public String sendNewPasswordEmail(String firstName, String lastName, String to, String key){
        
        this.message=("Kedves " + firstName + " " + lastName + "! \n \n"
                + "Ezt az e-mailt az elfelejtett jelszavad miatt küldtük. \n \n"
                + "A következő linken tudod megváltoztatni a jelszavad: \n \n"
                + "http://localhost:8080/password-reset/"+ key + "\n \n"
                + "Üdvözlettel MobileService csapata!");
        
        this.subject = "Jelszó reset";

        return emailSender.sendEmail(this.from, to, this.subject, this.message);
    }
}
