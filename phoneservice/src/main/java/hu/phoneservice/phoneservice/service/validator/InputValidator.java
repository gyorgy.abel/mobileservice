package hu.phoneservice.phoneservice.service.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InputValidator {

    private NameFormatValidator nameValidator;
    private EmailFormatValidator emailFormatValidator;
    private PasswordFormatValidator passwordValidator;
    private UuidValidator keyValidator;

    @Autowired
    public InputValidator(NameFormatValidator nameValidator,
                          EmailFormatValidator emailFormatValidator,
                          PasswordFormatValidator passwordValidator,
                          UuidValidator keyValidator) {
        this.nameValidator = nameValidator;
        this.emailFormatValidator = emailFormatValidator;
        this.passwordValidator = passwordValidator;
        this.keyValidator = keyValidator;
    }

    public InputValidator() {}
    
    public String companyNameCheck(String name) {
    
        if (name != null && !name.isEmpty()) {
            if (!this.nameValidator.isValidName(name)) {
                return "company_name_format_not_correct";
            }
            return "";
        }
        return "company_name_required";
    }
    
    public String nameCheck(String name) {

        if (name != null && !name.isEmpty()) {
            if (!this.nameValidator.isValidName(name)) {
                return "name_format_not_correct";
            }
            return "";
        }
        return "name_required";
    }

    public String emailCheck(String email) {

        if (email != null && !email.isEmpty()) {
            if (!this.emailFormatValidator.isValid(email)) {
                return "email_format_not_correct";
            }
            return "";
        }
        return "email_required";
    }

    public String newPasswordCheck(String password, String confirmPassword) {

        if (password != null && !password.isEmpty() &&
            confirmPassword != null && !confirmPassword.isEmpty()) {
            if (!this.passwordValidator.isValidPassword(password)) {
                return "password_format_not_correct";
            }
            if (!password.equals(confirmPassword)) {
                return "two_password_not_equals";
            }
            return "";
        }
        return "password_required";
    }
    
    public String passwordCheck(String password){
        
        if (password != null && !password.isEmpty()) {
            if (!this.passwordValidator.isValidPassword(password)) {
                return "password_format_not_correct";
            }
            return "";
        }
        return "password_required";
        
    }
    
    public String keyCheck(String key) {

        if (key != null && !key.isEmpty()) {

            if (!keyValidator.isValidKey(key)) {
                return "key_format_not_correct";
            }
            return "";
        }
        return "key_not_empty";
    }

    public void setNameValidator(NameFormatValidator nameValidator) {
        this.nameValidator = nameValidator;
    }

    public void setEmailFormatValidator(EmailFormatValidator emailFormatValidator) {
        this.emailFormatValidator = emailFormatValidator;
    }

    public void setPasswordValidator(PasswordFormatValidator passwordValidator) {
        this.passwordValidator = passwordValidator;
    }

    public void setKeyValidator(UuidValidator keyValidator) {
        this.keyValidator = keyValidator;
    }
}
