package hu.phoneservice.phoneservice.service.user;

import hu.phoneservice.phoneservice.entity.User;
import hu.phoneservice.phoneservice.repositroy.UserRepository;
import hu.phoneservice.phoneservice.service.validator.InputValidator;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserActivationService {

    private UserRepository userDao;
    private InputValidator inputValidator;

    @Autowired
    public UserActivationService(UserRepository userDao, InputValidator inputValidator) {
        this.userDao = userDao;
        this.inputValidator = inputValidator;
    }

    public String userActivation(String activationKey) {

        String keyValidator = inputValidator.keyCheck(activationKey);

        if (!keyValidator.isEmpty()) {
            return keyValidator;
        }

        Optional<User> user = userDao.findByActivation(activationKey);

        if (!user.isPresent()) {
            return "key_not_found";
        }

        user.get().setActivation("");
        userDao.save(user.get());
        return "activation_success";
    }
}
