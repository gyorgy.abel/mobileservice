package hu.phoneservice.phoneservice.entity;

import hu.phoneservice.phoneservice.enums.WorkSheetStatus;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.ColumnTransformer;

@Getter
@Setter
@EqualsAndHashCode(of = "worksheetId")
@ToString
@Entity
@Table(name = "worksheets")
public class Worksheet implements Serializable {

    static final long serialVersionUID = 15L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", unique = true, nullable = false)
    private Integer worksheetId;
    
    @ColumnTransformer(
            forColumn = "phone_type",
            read = "pgp_sym_decrypt(phone_type,'${DB_KEY}')",
            write = "pgp_sym_encrypt(?,'${DB_KEY}')"
    )
    @Column(name = "phone_type", length = 200, nullable = false, columnDefinition = "bytea")
    private String phoneType;

    @ColumnTransformer(
            forColumn = "imei_number",
            read = "pgp_sym_decrypt(imei_number,'${DB_KEY}')",
            write = "pgp_sym_encrypt(?,'${DB_KEY}')"
    )
    @Column(name = "imei_number", length = 100, nullable = false, columnDefinition = "bytea")
    private Long imeiNumber;

    @Column(name = "issue_description", length = 4000, nullable = true, columnDefinition = "TEXT")
    private String issueDescription;

    @Enumerated(EnumType.STRING)
    @Column(name = "worksheet_status")
    private WorkSheetStatus worksheetStatus;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "user_id", nullable = false, insertable = false, updatable = false)
    private User userId;

    @Column(name = "created_at", nullable = false, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at", nullable = true, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private ZonedDateTime updatedAt;

    @Column(name = "deleted_at", nullable = true, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private ZonedDateTime deletedAt;
}
