package hu.phoneservice.phoneservice.entity;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.ColumnTransformer;

@Getter
@Setter
@EqualsAndHashCode(of = "userId")
@ToString
@Entity
@Table(name = "users")
public class User implements Serializable{

    static final long serialVersionUID = 2L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", unique = true, nullable = false)
    private Long userId;

    @ColumnTransformer(
            forColumn = "email_address",
            read = "pgp_sym_decrypt(email_address,'${DB_KEY}')",
            write = "pgp_sym_encrypt(?,'${DB_KEY}')"
    )
    @Column(name = "email_address", unique = true, nullable = false, columnDefinition = "bytea")
    private String emailAddress;

    @Column(name = "password", length = 200, nullable = false)
    private String password;

    @ColumnTransformer(
            forColumn = "first_name",
            read = "pgp_sym_decrypt(first_name,'${DB_KEY}')",
            write = "pgp_sym_encrypt(?,'${DB_KEY}')"
    )
    @Column(name = "first_name", nullable = false, columnDefinition = "bytea")
    private String firstName;

    @ColumnTransformer(
            forColumn = "last_name",
            read = "pgp_sym_decrypt(last_name,'${DB_KEY}')",
            write = "pgp_sym_encrypt(?,'${DB_KEY}')"
    )
    @Column(name = "last_name", nullable = false, columnDefinition = "bytea")
    private String lastName;
    
    @Column(name = "activation", length = 36, nullable = true)
    private String activation;
    
    @Column(name = "forgot_password_key", length = 36, nullable = true)
    private String forgotPasswordKey;
    
    @OneToMany(mappedBy = "userId", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Worksheet> workSheets;
    
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_roles",
            joinColumns = {
                @JoinColumn(name = "user_id")},
            inverseJoinColumns = {
                @JoinColumn(name = "role_id")}
    )
    private Set<Role> userRoles = new HashSet<>();

    public void addRoles(String roleName) {
        if (this.userRoles == null || this.userRoles.isEmpty()) {
            this.userRoles = new HashSet<>();
        }
        this.userRoles.add(new Role(roleName));
    }
    
    @Column(name = "created_at", nullable = false, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private ZonedDateTime createdAt;

    @Column(name = "updated_at", nullable = true, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private ZonedDateTime updatedAt;

    @Column(name = "deleted_at", nullable = true, columnDefinition = "TIMESTAMP WITH TIME ZONE")
    private ZonedDateTime deletedAt;
}
