package hu.phoneservice.phoneservice.generator;

import java.util.UUID;
import org.springframework.stereotype.Component;

@Component
public class UuidGenerator {

    public String uuidGenerator(){
        return UUID.randomUUID().toString();
    }
}
